package com.isj.services;

import javax.servlet.http.HttpServletRequest;

import com.isj.entities.Role;
import com.isj.entities.User;
import com.isj.entities.RoleEnum;
import com.isj.repository.RoleRepository;
import com.isj.repository.UserRepository;
import com.isj.config.JwtTokenProvider;
import com.isj.utils.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;


@Service
public class AutheticationService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    public String signin(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return jwtTokenProvider.generateToken(username, userRepository.findByLogin(username).getAllRoles());
        } catch (final AuthenticationException e) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public String signup(User user) {
        if (!userRepository.existsByLogin(user.getLogin())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByRole(RoleEnum.ROLE_USER));
            user.setRoles(roles);
            userRepository.save(user);
            return jwtTokenProvider.generateToken(user.getLogin(), user.getAllRoles());
        } else {
            throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void delete(String login) {
        userRepository.deleteByLogin(login);
    }

    public User search(String login) {
        final User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
        }
        return user;
    }

    public User whoami(HttpServletRequest req) {
        return userRepository.findByLogin(jwtTokenProvider.extractLogin(jwtTokenProvider.extractTokenFromRequest(req)));
    }

    public String refresh(String login) {
        User user = userRepository.findByLogin(login);
        if(user != null){
            return jwtTokenProvider.generateToken(login, user.getAllRoles());
        }
        throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
    }
}
