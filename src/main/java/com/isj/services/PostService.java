package com.isj.services;


import com.isj.config.JwtTokenProvider;
import com.isj.dto.CovoiturageDataDto;
import com.isj.dto.ResponseDto;
import com.isj.entities.Covoiturage;
import com.isj.entities.TypeCovoiturage;
import com.isj.entities.TypeCovoiturageEnum;
import com.isj.entities.User;
import com.isj.repository.CovoiturageRepository;
import com.isj.repository.TypeCovoiturageRepository;
import com.isj.repository.UserRepository;
import com.isj.utils.exception.CustomException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class PostService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CovoiturageRepository covoiturageRepository;

    @Autowired
    private TypeCovoiturageRepository typeCovoiturageRepository;


    public ResponseDto postCovoiturageService(String token, CovoiturageDataDto covoiturageDto){
        ResponseDto response = new ResponseDto();
        User user;
        Covoiturage covoiturage;
        // Validate the token
        if(!jwtTokenProvider.validateToken(token)){
            throw new CustomException("Expired or invalid JWT token", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        user = userRepository.findByLogin(jwtTokenProvider.extractLogin(token));
        if(user == null){
            throw new CustomException("Expired or invalid JWT token", HttpStatus.NOT_FOUND);
        }

        // We get the pooling type from the db
        TypeCovoiturage typeCovoiturage = typeCovoiturageRepository.findByTypeCovoiturage(
                TypeCovoiturageEnum.valueOf(covoiturageDto.getType())
        );
        if(typeCovoiturage == null){
            throw new CustomException("Le type de covoiturage est inexistant", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // We skip the mapping of the field Type
        modelMapper.addMappings(new PropertyMap<Covoiturage, CovoiturageDataDto>() {
            @Override
            protected void configure(){
                skip().setType(null);
            }
        });
        covoiturage = this.modelMapper.map(covoiturageDto, Covoiturage.class);
        covoiturage.setPrixService(covoiturageDto.getPrixService());
        covoiturage.setType(typeCovoiturage);
        covoiturage.setUser(user);
        try{
            covoiturageRepository.save(covoiturage);
            response.setError("false");
            response.setMessage("Offre publié avec succès ");
            response.setHttpStatus( HttpStatus.CREATED);
        }catch (Exception e){
            response.setError("true");
            response.setMessage("Une erreur s'est produite");
            response.setHttpStatus( HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }


}
