package com.isj.controller;


import com.isj.dto.UserDataDto;
import com.isj.entities.User;
import com.isj.models.AuthenticationRequest;
import com.isj.services.AutheticationService;
import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@Api(tags = "users")
public class UserController {

    @Autowired
    private AutheticationService autheticationService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/signin")
    @ApiOperation(value = "${UserController.signin}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 422, message = "Invalid username/password supplied")})
    public String login(
            @ApiParam(" Login && Password ") @RequestBody AuthenticationRequest auth) {
        System.out.println(auth.getPassword());
        return autheticationService.signin(auth.getLogin(), auth.getPassword());
    }

    @PostMapping("/signup")
    @ApiOperation(value = "${UserController.signup}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 422, message = "Username is already in use"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")
        }
    )
    public String signup(
            @ApiParam(" User Data") @RequestBody UserDataDto user) {
        return autheticationService.signup(modelMapper.map(user, User.class));
    }
}
