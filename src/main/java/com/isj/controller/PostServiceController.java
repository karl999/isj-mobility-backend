package com.isj.controller;


import com.isj.config.JwtTokenProvider;
import com.isj.dto.CovoiturageDataDto;
import com.isj.dto.ResponseDto;
import com.isj.services.PostService;
import io.swagger.annotations.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/post")
@Api(tags = "post services")
public class PostServiceController {

    @Autowired
    private PostService postService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping(path = "covoiturage")
    @Authorization("ROLE_PROMOTEUR")
    @ApiOperation(value = "${PostServiceController.PostCovoiturage}", notes = "create a new carpooling offer")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 422, message = "Username is already in use"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")
        }
    )
    public ResponseDto postCovoiturage(HttpServletRequest request, @RequestBody CovoiturageDataDto covoiturageDto){
        String token = jwtTokenProvider.extractTokenFromRequest(request);
        return postService.postCovoiturageService(token, covoiturageDto);
    }


}
