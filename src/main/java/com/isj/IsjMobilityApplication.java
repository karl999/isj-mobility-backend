package com.isj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsjMobilityApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsjMobilityApplication.class, args);
	}

}
