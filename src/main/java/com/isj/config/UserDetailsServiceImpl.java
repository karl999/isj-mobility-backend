package com.isj.config;

import javax.transaction.Transactional;

import com.isj.entities.User;
import com.isj.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException{
        User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException("User Not found");
        }
         
        return org.springframework.security.core.userdetails.User//
                .withUsername(login)//
                .password(user.getPassword())//
                .authorities(user.getAuthorities())//
                .accountExpired(false)//
                .accountLocked(false)//
                .credentialsExpired(false)//
                .disabled(false)//
                .build();
    }

}
