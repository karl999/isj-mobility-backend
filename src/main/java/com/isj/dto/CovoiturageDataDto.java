package com.isj.dto;

import com.isj.entities.TypeCovoiturage;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.sql.Time;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CovoiturageDataDto {

    @ApiModelProperty(position = 0)
    private String type;

    @ApiModelProperty(position = 1)
    private int placesDispo;

    @ApiModelProperty(position = 2)
    private double prixService;

    @ApiModelProperty(position = 3)
    private String lieuDepart;

    @ApiModelProperty(position = 4)
    private Time horaireDepart;

    @ApiModelProperty(position = 5)
    private String lieuArrivee;

    @ApiModelProperty(position = 6)
    private Time horaireArrivee;


}
