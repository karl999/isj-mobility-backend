package com.isj.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDataDto {

    @ApiModelProperty(position = 0)
    private String name;

    @ApiModelProperty(position = 1)
    private String firstName;

    @ApiModelProperty(position = 2)
    private String login;

    @ApiModelProperty(position = 3)
    private String password;

    @ApiModelProperty(position = 4)
    private String email;

    @ApiModelProperty(position = 5)
    private Date dateN;
}
