package com.isj.entities;

public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER, ROLE_PROMOTEUR
}
