package com.isj.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Time;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Transport extends Livraison {

    @NotEmpty(message = "Veuillez renseigner le point de départ")
    private String lieuDepart;

    @NotEmpty(message = "Veuillez renseigner l'horaire' de départ")
    private Time horaireDepart;


}
