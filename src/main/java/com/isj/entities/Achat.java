package com.isj.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Achat extends Livraison {

    @NotEmpty(message = "Veuillez renseigner le lieu d'achat")
    private String lieuAchat;

    @NotEmpty(message = "Veuillez renseigner le prix du produit")
    private double prixProduit;

}
