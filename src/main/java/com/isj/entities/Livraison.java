package com.isj.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class Livraison extends Service {

    @NotEmpty(message = "Veuillez renseigner le nom du produit")
    private String produit;

    @NotEmpty(message = "Veuillez renseigner la catégorie du produit")
    private String categorieProduit;

    @NotEmpty(message = "Veuillez renseigner la quantité")
    private int quantite;
}
