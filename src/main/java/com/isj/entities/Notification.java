package com.isj.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idNotif;

    @NotEmpty(message = "Veuillez renseigner le prix du service")
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    private Service service;

}
