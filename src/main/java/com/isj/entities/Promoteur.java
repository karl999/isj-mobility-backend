package com.isj.entities;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class Promoteur {
     
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @Column(name = "nom")
    @NotEmpty(message = "Veuillez renseigner votre nom")
    protected String name;

    @Column(name = "prenom")
    @NotEmpty(message = "Veuillez renseigner votre prénom")
    protected String firstName;

    @Length(min = 5, message = "votre login doit avoir au moins 5 caractères")
    @NotEmpty(message = "Please provide your username")
    protected String login;

    @Length(min = 5, message = "votre mot de passe doit avoir au moins 5 caractères")
    @NotEmpty(message = "Please provide your password")
    protected String password;

    @Email(message = "Veuillez renseigner une adresse email valide")
    @NotEmpty(message = "Veuillez renseigner une adresse email")
    protected String email;

    // Birthday
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateN;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
        joinColumns = @JoinColumn(name = "id_user"),
        inverseJoinColumns = @JoinColumn(name = "id_role")
    )
    protected Set<Role> roles = new HashSet<>();


}
