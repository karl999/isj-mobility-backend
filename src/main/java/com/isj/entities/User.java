package com.isj.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "user",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "login"),
            @UniqueConstraint(columnNames = "email")
        }
    )
public class User extends Promoteur {

    public Set<RoleEnum> getAllRoles(){
        Set<RoleEnum> roleEnums = new HashSet<>();
        for (Role role: this.getRoles()) {
            roleEnums.add(role.getRole());
        }
        return roleEnums;
    }

    public Collection<? extends GrantedAuthority> getAuthorities(){
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>(this.getRoles().size());

        for (Role role : this.getRoles()){
            authorities.add(new SimpleGrantedAuthority(role.getRole().name()));
        }

        return authorities;
    }


}
