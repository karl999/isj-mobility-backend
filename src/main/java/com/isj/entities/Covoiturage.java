package com.isj.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Time;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Covoiturage extends Service {

    @ManyToOne(fetch = FetchType.LAZY)
    @NotEmpty(message = "Veuillez renseigner type de transport")
    private TypeCovoiturage type;

    @NotEmpty(message = "Veuillez renseigner le nombre de places disponibles")
    private int placesDispo;

    @NotEmpty(message = "Veuillez renseigner le point de départ")
    private String lieuDepart;

    @NotEmpty(message = "Veuillez renseigner l'horaire' de départ")
    private Time horaireDepart;

    @NotEmpty(message = "Veuillez renseigner le point d'arrivée")
    private String lieuArrivee;

    @NotEmpty(message = "Veuillez renseigner l'horaire de départ")
    private Time horaireArrivee;

}
