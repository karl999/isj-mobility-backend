package com.isj.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TypeCovoiturage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTypeCovoiturage;

    @NotEmpty
    @Enumerated(EnumType.STRING)
    private TypeCovoiturageEnum typeCovoiturage;
}
