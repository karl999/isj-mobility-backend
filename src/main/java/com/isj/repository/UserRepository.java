package com.isj.repository;


import javax.transaction.Transactional;

import com.isj.entities.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository <User, Long> {
    
    User findByLogin(String login);
    User findByEmail(String email);


    Boolean existsByLogin(String login);
    Boolean existsByEmail(String email);

    @Transactional
    Boolean deleteByLogin(String login);
    @Transactional
    Boolean deleteByEmail(String email);
}
