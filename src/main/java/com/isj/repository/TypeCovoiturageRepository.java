package com.isj.repository;


import com.isj.entities.TypeCovoiturage;
import com.isj.entities.TypeCovoiturageEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TypeCovoiturageRepository extends JpaRepository<TypeCovoiturage, Long> {

    TypeCovoiturage findByTypeCovoiturage(TypeCovoiturageEnum typeCovoiturage);

}
