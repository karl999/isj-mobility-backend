package com.isj.repository;

import com.isj.entities.Covoiturage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CovoiturageRepository extends JpaRepository<Covoiturage, Long> {


}
